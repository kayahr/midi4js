/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */

package de.ailis.midi4js;

import javax.sound.midi.MidiMessage;

/**
 * Raw MIDI message. The messages are encoded in JavaScript so this class
 * is only used to pass the preprocessed data to the MIDI system.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */
public final class RawMidiMessage extends MidiMessage
{
    /**
     * Constructor
     * 
     * @param data
     *            The message data.
     */
    public RawMidiMessage(byte[] data)
    {
        super(data);
    }

    /**
     * @see javax.sound.midi.MidiMessage#clone()
     */
    @Override
    public Object clone()
    {
        return new RawMidiMessage(this.data.clone());
    }
}
