midi4js
=======

Description
-----------

midi4js enables JavaScript to communicate with attached MIDI devices.
A Java applet is used as a bridge between JavaScript and the Java MIDI
system. This project is currently an early alpha version. More
documentation will follow when the project is finished.

License
-------

This library is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Lesser General Public License][1] for more details.

[1]: http://kayahr.github.com/twodee/license.html "GNU Lesser General Public License"
