/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @use midi4js/Info.js
 * @use midi4js/MidiDevice.js
 * @use midi4js/MidiMessage.js
 */

/** 
 * @license
 * midi4js - Midi support for JavaScript
 * http://kayahr.github.com/midi4js
 * 
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *  */

/** 
 * The midi4js namespace
 * @type {Object}
 */
var midi4js = {};

/**
 * The jquery interface to the midi4js namespace.
 * @type {jQueryObject}
 */
var $midi4js = jQuery(midi4js);

/**
 * This flag is set to true when the applet is ready.
 * @type {boolean}
 */
midi4js.appletReady = false;

/**
 * The cached reference to the midi4js applet.
 * @type {?HTMLElement}
 */
midi4js.applet = null;

/**
 * Derives subClass from superClass.
 * 
 * @param {Function} subClass
 *            The sub class
 * @param {Function} superClass
 *            The super class
 */
midi4js.inherit = function(subClass, superClass)
{  
    var tmp = superClass.prototype;
    superClass = new Function();
    superClass.prototype = tmp;
    subClass.prototype = new superClass();
    subClass.prototype.constructor = subClass;
};

/**
 * Returns the midi4js applet. Throws an exception when the applet is not
 * yet ready.
 * 
 * @return {!HTMLElement}
 *            The midi4js applet.
 * @private
 */
midi4js.getApplet = function()
{
    if (!midi4js.appletReady) throw new Error("midi4js applet not ready yet");
    if (midi4js.applet) return midi4js.applet;
    /*@cc_on if (true)
    midi4js.applet = document.getElementById("midi4js-object");
    @else @*/
    midi4js.applet = document.getElementById("midi4js-embed");
    /*@end @*/
    if (!midi4js.applet) throw new Error("Can't gind midi4js applet");
    return midi4js.applet;
}

/**
 * Called by the midi4js applet when the applet has been started.
 * This function just calls another method by invoking a timer. This
 * makes sure the message processing takes place in the JavaScript thread.
 */
midi4js.appletStarted = function()
{
    setTimeout(midi4js.processStart, 0);
};

/**
 * Processes the midi4js applet start.
 * 
 * @private
 */
midi4js.processStart = function()
{
    // Ignore call when already called
    if (midi4js.appletReady) return;
    
    // Mark applet as ready
    midi4js.appletReady = true;
    
    // Trigger "ready" event
    $midi4js.trigger("ready");
};

/**
 * Called by the Midi4JS applet when the applet has been stopped.
 */
midi4js.appletStopped = function()
{
};

/**
 * Called by the Midi4JS applet when the applet has received a MIDI message.
 * This function just calls another method by invoking a timer. This
 * makes sure the message processing takes place in the JavaScript thread.
 * 
 * @param {number} receiverHandle
 *            The handle of the target receiver.
 * @param {string} jsonMessageStr
 *            JSON encoded message.
 * @param {number} timeStamp
 */
midi4js.messageReceived = function(receiverHandle, jsonMessageStr, timeStamp)
{
    setTimeout(midi4js.processMessage.bind(window, receiverHandle,
        jsonMessageStr, timeStamp), 0);
};

/**
 * Processes a MIDI message received by the Midi4JS applet.
 * 
 * @param {number} receiverHandle
 *            The handle of the target receiver.
 * @param {string} jsonMessageStr
 *            JSON encoded message.
 * @param {number} timeStamp
 * @private
 */
midi4js.processMessage = function(receiverHandle, jsonMessageStr, timeStamp)
{
    var message, receiver;
    
    message = midi4js.MidiMessage.fromJSON(jsonMessageStr);
    receiver = midi4js.Receiver.getInstance(receiverHandle);
    receiver.send(message, timeStamp);
};

/**
 * Returns a list with device info objects for each detected midi device.
 * 
 * @return {!Array.<!midi4js.Info>}
 *            The list with device info objects for each detected midi device.
 */
midi4js.getMidiDeviceInfo = function()
{
    var infos, strData, jsonData, i, max;
    
    strData = (/** @type {string} */ midi4js.getApplet()["getMidiDeviceInfo"]());
    jsonData = (/** @type {!Array.<!Object>} */ window.JSON.parse(strData));
    max = jsonData.length;
    infos = new Array(max);
    for (i = 0; i < max; i += 1)
    {
        infos[i] = new midi4js.Info(jsonData[i]);
    }
    return infos;    
}

/**
 * Returns the midi device with the specified index. Must be released with
 * the release() method when no longer needed.
 * 
 * @param {number} index
 *            The device index.
 */
midi4js.getMidiDevice = function(index)
{
    var handle;
    
    handle = (/** @type {number} */ midi4js.getApplet()["getMidiDevice"](index));
    return new midi4js.MidiDevice(handle);
}
