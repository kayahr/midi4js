/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 * @require midi4js/MidiMessage.js
 */

/**
 * Constructs a short midi message.
 * 
 * @param {number} status
 *            The status byte.
 * @param {number=} data1
 *            Optional first data byte.
 * @param {number=} data2
 *            Optional second data byte.
 * @constructor
 * @extends {midi4js.MidiMessage}
 * @class
 * Midi message.
 */
midi4js.ShortMessage = function(status, data1, data2)
{
    midi4js.MidiMessage.call(this,
        midi4js.ShortMessage.buildData(status, data1, data2));
};
midi4js.inherit(midi4js.ShortMessage, midi4js.MidiMessage);

/**
 * Status byte for MIDI Time Code Quarter Frame message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.MIDI_TIME_CODE = 0xF1;

/**
 * Status byte for Song Position Pointer message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.SONG_POSITION_POINTER = 0xF2;

/**
 * Status byte for MIDI Song Select message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.SONG_SELECT = 0xF3;

/**
 * Status byte for Tune Request message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.TUNE_REQUEST = 0xF6;

/**
 * Status byte for End of System Exclusive message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.END_OF_EXCLUSIVE = 0xF7;

/**
 * Status byte for Timing Clock messagem.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.TIMING_CLOCK = 0xF8;

/**
 * Status byte for Start message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.START = 0xFA;

/**
 * Status byte for Continue message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.CONTINUE = 0xFB;

/**
 * Status byte for Stop message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.STOP = 0xFC;

/**
 * Status byte for Active Sensing message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.ACTIVE_SENSING = 0xFE;

/**
 * Status byte for System Reset message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.SYSTEM_RESET = 0xFF;

/**
 * Command value for Note Off message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.NOTE_OFF = 0x80;

/**
 * Command value for Note On message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.NOTE_ON = 0x90;

/**
 * Command value for Polyphonic Key Pressure (Aftertouch) message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.POLY_PRESSURE = 0xA0;

/**
 * Command value for Control Change message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.CONTROL_CHANGE = 0xB0;

/**
 * Command value for Program Change message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.PROGRAM_CHANGE = 0xC0;

/**
 * Command value for Channel Pressure (Aftertouch) message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.CHANNEL_PRESSURE = 0xD0;

/**
 * Command value for Pitch Bend message.
 * @const
 * @type {number}
 */
midi4js.ShortMessage.PITCH_BEND = 0xE0;


/**
 * Builds the short message data.
 * 
 * @param {number} status
 *            The status byte.
 * @param {number=} data1
 *            Optional first data byte. Defaults to 0.
 * @param {number=} data2
 *            Optional second data byte. Defaults to 0.
 * @return {!Array.<number>}
 *            The built data.
 */
midi4js.ShortMessage.buildData = function(status, data1, data2)
{
    if (data1 == null) data1 = 0;
    if (data2 == null) data2 = 0;
    switch (status)
    {
        case 0xF6:                      // Tune Request
        case 0xF7:                      // EOX
            // System real-time messages
        case 0xF8:                      // Timing Clock
        case 0xF9:                      // Undefined
        case 0xFA:                      // Start
        case 0xFB:                      // Continue
        case 0xFC:                      // Stop
        case 0xFD:                      // Undefined
        case 0xFE:                      // Active Sensing
        case 0xFF:                      // System Reset
            return [ status ];
            
        case 0xF1:                      // MTC Quarter Frame
        case 0xF3:                      // Song Select
            return [ status, data1 ];
            break;
            
        case 0xF2:                      // Song Position Pointer
            return [ status, data1, data2 ];
    }
    
    switch(status & 0xF0)
    {
        case 0x80:
        case 0x90:
        case 0xA0:
        case 0xB0:
        case 0xE0:
            return [ status, data1, data2 ];
        case 0xC0:
        case 0xD0:
            return [ status, data1 ];
            
        default:
            throw new Error("Unknown short message status: " + status);
    }
};

/**
 * Returns the channel.
 * 
 * @return {number} The channel.
 */
midi4js.ShortMessage.prototype.getChannel = function()
{
    return this.getStatus() & 0x0f;
};

/**
 * Returns the command.
 * 
 * @return {number} The command.
 */
midi4js.ShortMessage.prototype.getCommand = function()
{
    return this.getStatus() & 0xf0;
};

/**
 * Returns the first data byte.
 * 
 * @return {number} The first data byte.
 */
midi4js.ShortMessage.prototype.getData1 = function()
{
    if (this.data.length > 1)
        return this.data[1];
    else
        return 0;
};

/**
 * Returns the second data byte.
 * 
 * @return {number} The second data byte.
 */
midi4js.ShortMessage.prototype.getData2 = function()
{
    if (this.data.length > 2)
        return this.data[2];
    else
        return 0;
};

/**
 * Builds and returns a short message instance for the specified JSON string.
 * 
 * @param {string} json
 *            The JSON encoded MIDI message.
 * @return {!midi4js.ShortMessage}
 *            The built message.
 */
midi4js.ShortMessage.fromJSON = function(json)
{
    var messageClass, data;
    
    data = window.JSON.parse(json);
    return new midi4js.ShortMessage(
        (/** @type {number} */ data["status"]),
        (/** @type {number} */ data["data1"]),
        (/** @type {number} */ data["data2"]));
};