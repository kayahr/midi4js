/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 */

/**
 * Constructs a MIDI receiver.
 * 
 * @param {number=} handle
 *            The receiver handle. If not set then a new custom receiver
 *            is created and a handle is automatically generated.
 * @constructor
 * @class
 * MIDI receiver.
 */
midi4js.Receiver = function(handle)
{
    if (handle != null)
        this.handle = handle;
    else
        this.handle = (/** @type {number} */
            midi4js.getApplet()["createReceiver"]());
    midi4js.Receiver.instances["" + this.handle] = this;
};

/**
 * The receiver handle.
 * @private
 * @type {?number}
 */
midi4js.Receiver.prototype.handle = null;

/**
 * Map from handles to instances.
 * @private
 * @type {!Object.<string,!midi4js.Receiver>}
 */
midi4js.Receiver.instances = {};

/**
 * Closes this receiver. It can't be used after that.
 */
midi4js.Receiver.prototype.close = function()
{
    this.checkHandle();
    midi4js.getApplet()["closeReceiver"](this.handle);
    this.handle = null;
};

/**
 * Checks if receiver is not already released.
 * 
 * @private
 */
midi4js.Receiver.prototype.checkHandle = function()
{
    if (!this.handle) throw new Error("Receiver is released");
};

/**
 * Returns the receiver instance with the specified handle.
 * 
 * @param {number} handle
 *            The receiver handle.
 * @return {!midi4js.Receiver}
 *            The receiver instance.
 */
midi4js.Receiver.getInstance = function(handle)
{
    var receiver = midi4js.Receiver.instances["" + handle];
    if (!receiver)
        receiver = new midi4js.Receiver(handle);
    return receiver;
};

/**
 * Returns the handle of this receiver. This is only used internally.
 * 
 * @return {number} The receiver handle.
 */
midi4js.Receiver.prototype.getHandle = function()
{
    if (!this.handle) throw new Error("Receiver is released");
    return this.handle;
};

/**
 * This method receives a message which was send from the transmitter this
 * receiver is connected to. Overwrite this method to implement your own
 * message handler. 
 *
 * @param {!midi4js.MidiMessage} message
 *            The midi message.
 * @param {number} timeStamp
 *            The message time stamp.
 */
midi4js.Receiver.prototype.send = function(message, timeStamp) {};
