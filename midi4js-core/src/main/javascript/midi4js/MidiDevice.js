/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 * @use midi4js/Transmitter.js
 * @use midi4js/Receiver.js
 */

/**
 * Constructs a midi device.
 * 
 * @param {number} handle
 *            The device handle.
 * 
 * @constructor
 * @class
 * Midi device.
 */
midi4js.MidiDevice = function(handle)
{
    this.handle = handle;    
};

/**
 * @private
 * @type {?number}
 */
midi4js.MidiDevice.prototype.handle = null;

/**
 * Releases the device.
 */
midi4js.MidiDevice.prototype.release = function()
{
    this.checkHandle();
    midi4js.getApplet()["releaseMidiDevice"](this.handle);
    this.handle = null;
};

/**
 * Checks if device is not already released.
 * 
 * @private
 */
midi4js.MidiDevice.prototype.checkHandle = function()
{
    if (!this.handle) throw new Error("Device is released");
};

/**
 * Returns the maximum number of allowed receivers. -1 indicates an unlimited
 * number.
 * 
 * @return {number} The number of allowed receivers. -1 means unlimited.
 */
midi4js.MidiDevice.prototype.getMaxReceivers = function()
{
    this.checkHandle();
    return (/** @type {number} */ midi4js.getApplet()["getMaxReceivers"](this.handle));
};

/**
 * Returns the maximum number of allowed transmitters. -1 indicates an unlimited
 * number.
 * 
 * @return {number} The number of allowed transmitters. -1 means unlimited.
 */
midi4js.MidiDevice.prototype.getMaxTransmitters = function()
{
    this.checkHandle();
    return (/** @type {number} */ midi4js.getApplet()["getMaxTransmitters"](this.handle));
};

/**
 * Returns a MIDI OUT connection from the MIDI device.
 * 
 * @return {!midi4js.Transmitter}
 *             The transmitter.
 */
midi4js.MidiDevice.prototype.getTransmitter = function()
{
    var handle;
    
    this.checkHandle();
    handle = (/** @type {number} */ midi4js.getApplet()["getTransmitter"](this.handle));
    return midi4js.Transmitter.getInstance(handle);
};

/**
 * Returns all active transmitters.
 * 
 * @return {!Array.<!midi4js.Transmitter>}
 *             The active transmitters.
 */
midi4js.MidiDevice.prototype.getTransmitters = function()
{
    var json, handles, transmitters, max, i;
    
    this.checkHandle();
    json = (/** @type {string} */ midi4js.getApplet()["getTransmitters"](this.handle));
    handles = (/** @type {!Array.<number>} */ window.JSON.parse(json));
    max = handles.length;
    transmitters = new Array(max);
    for (i = 0; i < max; i += 1)
        transmitters[i] = midi4js.Transmitter.getInstance(handles[i]);
    return transmitters;
};

/**
 * Returns a MIDI OUT connection from the MIDI device.
 * 
 * @return {!midi4js.Receiver}
 *             The receiver.
 */
midi4js.MidiDevice.prototype.getReceiver = function()
{
    var handle;
    
    this.checkHandle();
    handle = (/** @type {number} */ midi4js.getApplet()["getReceiver"](this.handle));
    return midi4js.Receiver.getInstance(handle);
};

/**
 * Returns all active receivers.
 * 
 * @return {!Array.<!midi4js.Receiver>}
 *             The active receivers.
 */
midi4js.MidiDevice.prototype.getReceivers = function()
{
    var json, handles, receivers, max, i;
    
    this.checkHandle();
    json = (/** @type {string} */ midi4js.getApplet()["getReceivers"](this.handle));
    handles = (/** @type {!Array.<number>} */ window.JSON.parse(json));
    max = handles.length;
    receivers = new Array(max);
    for (i = 0; i < max; i += 1)
        receivers[i] = midi4js.Receiver.getInstance(handles[i]);
    return receivers;
};

/**
 * Opens the device. It must be closed when no longer needed.
 */
midi4js.MidiDevice.prototype.open = function()
{
    this.checkHandle();
    midi4js.getApplet()["openMidiDevice"](this.handle);
};

/**
 * Closes the device and releases this reference to it. A new reference must
 * be acquired if the device is to be used again.
 */
midi4js.MidiDevice.prototype.close = function()
{
    this.checkHandle();
    midi4js.getApplet()["closeMidiDevice"](this.handle);
};

/**
 * Checks if device is open.
 * 
 * @return {boolean}
 *            True if device is open, false if not.
 */
midi4js.MidiDevice.prototype.isOpen = function()
{
    this.checkHandle();
    return (/** @type {boolean} */ 
        midi4js.getApplet()["isMidiDeviceOpen"](this.handle));
};
