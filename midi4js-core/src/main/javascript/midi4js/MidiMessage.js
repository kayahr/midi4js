/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 * @use midi4js/ShortMessage.js
 */

/**
 * Constructs a midi message.
 * 
 * @param {!Array.<number>} data
 *            The message data.
 * 
 * @constructor
 * @protected
 * @class
 * Midi message.
 */
midi4js.MidiMessage = function(data)
{
    this.setMessage(data);
};

/**
 * The message data.
 * @protected
 * @type {!Array.<number>}
 */
midi4js.MidiMessage.prototype.data;

/**
 * Sets the MIDI message data.
 * 
 * @param {!Array.<number>} data
 *            The message data.
 * @protected
 */
midi4js.MidiMessage.prototype.setMessage = function(data)
{
    this.data = data.splice(0);    
};

/**
 * Returns the MIDI message data.
 * 
 * @return {!Array.<number>} 
 *            The MIDI message data.
 */
midi4js.MidiMessage.prototype.getMessage = function()
{
    return this.data.splice(0);
};

/**
 * Returns the status byte of the MIDI message.
 * 
 * @return {number} The status byte.
 */
midi4js.MidiMessage.prototype.getStatus = function()
{
    if (!this.data.length) return 0;
    return this.data[0];
};

/**
 * Builds and returns a MIDI message instance for the specified JSON string.
 * 
 * @param {string} json
 *            The JSON encoded MIDI message.
 * @return {!midi4js.MidiMessage}
 *            The built message.
 */
midi4js.MidiMessage.fromJSON = function(json)
{
    var messageClass, data;
    
    data = window.JSON.parse(json);
    messageClass = (/** @type {string} */ data["class"]);
    if (messageClass == "ShortMessage")
        return midi4js.ShortMessage.fromJSON(json);
    else
        return new midi4js.MidiMessage(data["message"]);
};