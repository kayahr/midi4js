/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 */

/**
 * Constructs a MIDI transmitter.
 * 
 * @param {number} handle
 *            The transmitter.
 * 
 * @constructor
 * @class
 * MIDI transmitter.
 */
midi4js.Transmitter = function(handle)
{
    this.handle = handle;    
};

/**
 * The transmitter handle.
 * @private
 * @type {?number}
 */
midi4js.Transmitter.prototype.handle = null;

/**
 * Map from handles to instances.
 * @private
 * @type {!Object.<string,!midi4js.Transmitter>}
 */
midi4js.Transmitter.instances = {};

/**
 * Closes this transmitter. It can't be used after that.
 */
midi4js.Transmitter.prototype.close = function()
{
    this.checkHandle();
    midi4js.getApplet()["closeTransmitter"](this.handle);
    this.handle = null;
};

/**
 * Checks if transmitter is not already released.
 * 
 * @private
 */
midi4js.Transmitter.prototype.checkHandle = function()
{
    if (!this.handle) throw new Error("Transmitter is released");
};

/**
 * Returns the transmitter instance with the specified handle.
 * 
 * @param {number} handle
 *            The transmitter handle.
 * @return {!midi4js.Transmitter}
 *            The transmitter instance.
 */
midi4js.Transmitter.getInstance = function(handle)
{
    var transmitter = midi4js.Transmitter.instances["" + handle];
    if (!transmitter)
    {
        transmitter = new midi4js.Transmitter(handle);
        midi4js.Transmitter.instances["" + handle] = transmitter;
    }
    return transmitter;
};

/**
 * Returns the receiver which is connected to this transmitter. Null if none.
 * 
 * @return {?midi4js.Receiver}
 *            The connected receiver or null of none.
 */
midi4js.Transmitter.prototype.getReceiver = function()
{
    var handle;
    
    this.checkHandle();
    handle = (/** @type {number} */
        midi4js.getApplet()["getTransmitterReceiver"](this.handle));
    if (!handle) return null;
    return midi4js.Receiver.getInstance(handle);
};

/**
 * Connects the specified receiver to this transmitter.
 * 
 * @param {?midi4js.Receiver} receiver
 *           The receiver to connect or null to disconnect current one.
 */
midi4js.Transmitter.prototype.setReceiver = function(receiver)
{
    this.checkHandle();
    midi4js.getApplet()["setTransmitterReceiver"](this.handle,
        receiver.getHandle());    
};
