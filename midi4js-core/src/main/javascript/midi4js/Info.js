/**
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 * 
 * @require midi4js.js
 */

/**
 * Constructs a midi device info object.
 * 
 * @param {!Object.<string, string>} json
 *            Device info as json string.
 * 
 * @constructor
 * @class
 * Midi device info.
 */
midi4js.Info = function(json)
{
    this.name = "" + json["name"];
    this.description = "" + json["description"];
    this.vendor = "" + json["vendor"];
    this.version = "" + json["version"];
};

/**
 * Returns the device name.
 * 
 * @return {string}
 *            The device name.
 */
midi4js.Info.prototype.getName = function()
{
    return this.name;
}

/**
 * Returns the device description.
 * 
 * @return {string}
 *            The device description.
 */
midi4js.Info.prototype.getDescription = function()
{
    return this.description;
};

/**
 * Returns the device vendor name.
 * 
 * @return {string}
 *            The device vendor name.
 */
midi4js.Info.prototype.getVendor = function()
{
    return this.vendor;
};

/**
 * Returns the device version.
 * 
 * @return {string}
 *            The device version.
 */
midi4js.Info.prototype.getVersion = function()
{
    return this.version;
};
