<?php require "../../../target/demo/resolver.php" ?>
<!DOCTYPE html>
<html>
  <head>
    <script type="text/javascript" src="scripts/standards/es5-shim.js"></script>
    <script type="text/javascript" src="scripts/standards/console-shim.js"></script>
    <script type="text/javascript" src="scripts/jquery/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/midi4js.css" />
    <!--[if lt IE 9]><script src="scripts/standards/IE9.js">IE7_PNG_SUFFIX=".png";</script><![endif]-->
    <?php $resolver->includeScript("midi4js.js"); ?>
    <script type="text/javascript">

    $(function()
    {
        if (midi4js.ready)
            dump();
        else
            $midi4js.bind("ready", dump);
    });

    function dump()
    {
        var root, head, infos, i, max, info, device;

        root = $("#dump");
        infos = midi4js.getMidiDeviceInfo();
        for (i = 0, max = infos.length; i < max; i++)
        {
            info = infos[i];
            device = midi4js.getMidiDevice(i);

            var code = $(
                "<h2>" + info.getName() + "</h2>" +
                "<p>" +
                  "<strong>Description: </strong> " + info.getDescription() +
                  "<br />" +
                  "<strong>Version: </strong> " + info.getVersion() +
                  "<br />" +
                  "<strong>Vendor: </strong> " + info.getVendor() +
                  "<br />" +
                  "<strong>Max receivers: </strong> " + device.getMaxReceivers() +
                  "<br />" +
                  "<strong>Max transmitters: </strong> " + device.getMaxTransmitters() +
                  "<br />" +
                  "<strong>Last received message: </strong> <span class=\"lastMessage\"></span>" +
                "</p>"
            );
            root.append(code);
            window.code = code;
            var lastMessage = code.find(".lastMessage");

            if (device.getMaxTransmitters() && !(device.getMaxReceivers()))
            {
                  device.open();
                  var transmitter = device.getTransmitter();
                  var receiver = new midi4js.Receiver();
                  receiver.lastMessage = lastMessage;
                  receiver.send = function(message, timeStamp)
                  {
                      if (message.getStatus() == 0xfe) return;
                      this.lastMessage.text(decodeMessage(message));
                  }
                  transmitter.setReceiver(receiver);
            }
            else device.release();
        }
    };

    function decodeMessage(message)
    {
        if (message instanceof midi4js.ShortMessage)
        {
            return decodeShortMessage(message);
        }
        return "Unknown message class";
    }

    var sm_astrKeyNames = [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" ];

    var SYSTEM_MESSAGE_TEXT = [
    	  "System Exclusive (should not be in ShortMessage!)",
    		"MTC Quarter Frame: ",
    		"Song Position: ",
    		"Song Select: ",
    		"Undefined",
    		"Undefined",
    		"Tune Request",
    		"End of SysEx (should not be in ShortMessage!)",
    		"Timing clock",
    		"Undefined",
    		"Start",
    		"Continue",
    		"Stop",
    		"Undefined",
    		"Active Sensing",
    		"System Reset"
    ];

    var QUARTER_FRAME_MESSAGE_TEXT = [
    		"frame count LS: ",
    		"frame count MS: ",
    		"seconds count LS: ",
    		"seconds count MS: ",
    		"minutes count LS: ",
    		"minutes count MS: ",
    		"hours count LS: ",
    		"hours count MS: "
    ];

	  var FRAME_TYPE_TEXT = [
    		"24 frames/second",
    		"25 frames/second",
    		"30 frames/second (drop)",
    		"30 frames/second (non-drop)"
	  ];


    function getKeyName(nKeyNumber)
    {
        if (nKeyNumber > 127)
        {
            return "illegal value";
        }
        else
        {
            var nNote = nKeyNumber % 12;
            var nOctave = Math.floor(nKeyNumber / 12);
            return sm_astrKeyNames[nNote] + (nOctave - 1);
        }
    }

	  function get14bitValue(nLowerPart, nHigherPart)
	  {
		    return (nLowerPart & 0x7F) | ((nHigherPart & 0x7F) << 7);
	  }

    function decodeShortMessage(message)
    {
        var strMessage = null;
        switch (message.getCommand())
        {
            case 0x80:
                strMessage = "note Off " + getKeyName(message.getData1()) + " velocity: " + message.getData2();
                break;

            case 0x90:
                strMessage = "note On " + getKeyName(message.getData1()) + " velocity: " + message.getData2();
                break;

            case 0xa0:
                strMessage = "polyphonic key pressure " + getKeyName(message.getData1()) + " pressure: " + message.getData2();
                break;

            case 0xb0:
                strMessage = "control change " + message.getData1() + " value: " + message.getData2();
                break;

            case 0xc0:
                strMessage = "program change " + message.getData1();
                break;

            case 0xd0:
                strMessage = "key pressure " + getKeyName(message.getData1()) + " pressure: " + message.getData2();
                break;

            case 0xe0:
                strMessage = "pitch wheel change " + get14bitValue(message.getData1(), message.getData2());
                break;

            case 0xF0:
                strMessage = SYSTEM_MESSAGE_TEXT[message.getChannel()];
                switch (message.getChannel())
                {
                    case 0x1:
                        var nQType = (message.getData1() & 0x70) >> 4;
                        var nQData = message.getData1() & 0x0F;
                        if (nQType == 7)
                        {
                            nQData = nQData & 0x1;
                        }
                        strMessage += QUARTER_FRAME_MESSAGE_TEXT[nQType] + nQData;
                        if (nQType == 7)
                        {
                          var nFrameType = (message.getData1() & 0x06) >> 1;
                          strMessage += ", frame type: " + FRAME_TYPE_TEXT[nFrameType];
                        }
                        break;

                    case 0x2:
                        strMessage += get14bitValue(message.getData1(), message.getData2());
                        break;

                    case 0x3:
                        strMessage += message.getData1();
                        break;
                  }
                  break;

            default:
                strMessage = "unknown message: status = " + message.getStatus() + ", byte1 = " + message.getData1() + ", byte2 = " + message.getData2();
                break;
        }
        if (message.getCommand() != 0xF0)
        {
            var nChannel = message.getChannel() + 1;
            var strChannel = "channel " + nChannel + ": ";
            strMessage = strChannel + strMessage;
        }
        return strMessage;
    }

    </script>
  </head>
  <body>

    <h1>Midi4JS Dump demo</h1>

    <div id="dump"></div>

    <div id="midi4js-applet">
      <object id="midi4js-object"
              classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
              codebase="http://java.sun.com/update/1.6.0/jinstall-6u30-windows-i586.cab#Version=1,6,0,0">
        <param name="archive" value="midi4js.jar" />
        <param name="code" value="../../../../midi4js-applet/target/midi4js-applet-1.0.0-SNAPSHOT.jar" />
        <embed id="midi4js-embed"
               type="application/x-java-applet;version=1.6"
               archive="../../../../midi4js-applet/target/midi4js-applet-1.0.0-SNAPSHOT.jar"
               code="de.ailis.midi4js.Midi4JS"
               pluginspage="http://java.com/download/"
               onstart="midi4js.appletStarted()" />
      </object>
    </div>

  </body>
</html>
